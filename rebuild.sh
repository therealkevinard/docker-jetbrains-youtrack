#!/usr/bin/env bash

# remove, recreate, prepare directories
sudo rm -rf docker/data docker/conf docker/logs docker/backups
mkdir -p -m 750 docker/data docker/conf docker/logs docker/backups
sudo chown -R 13001:13001 docker/data docker/conf docker/logs docker/backups
docker-compose up

# access setup wizard at [eg] http://youtrack.docker.localhost:81/?wizard_token=[token returned by init command]